# LogDNA Senior FrontEnd Engineer Challenge

## Implementation Details
I built the application in Preact (a fully API compatible implementation of React), Parcel, Typescript, and Sass. I had an existing project boilerplate that I leverage to bootstrap the typescript configuration, build, and compile scripts. The portal, modal, and a11y hooks are existing modules that I built previously and ported into the Pokédex implementation.

I built the store to be cache aware, so previously clicked Pokemon type pages should hit the object store instead of attempting to make a network fetch

There are few things that I didn’t get to given the time constraints that I believe would be important if this was a shipping application.

1. Tests – Unit tests should be implemented for the Pokédex store as well as the hooks, observable, and fuzzy search modules. Integration tests should be implemented for the components and views.
2. Error State – There is minimal error handling in the implementation, none of which are being communicated to the end user. Something like toast notifications for errors would be ideal if for instance a network call failed.
3. Loading States – The best approach here would be to implement skeletal loading of each of the views when data is being fetched. This would communicate to the user that the content is being fetched. It would also help avoid “DOM trashing” when the content is render since the component layout would be rendered and would avoid major reflow events.
4. Mobile friendliness – I made an assumption and focused only on a desktop layout. The layout does not support smaller view ports.

I also was not sure if the header, nav, and aside should be fixed. I made an assumption that they should not, so the entire content is scrollable.


You can see the production version [here](https://pokedex-preact.vercel.app/)

## Running Local
`yarn install && yarn dev`

## Task

**Build a Pokedex!**
Using the Pokémon API (https://pokeapi.co/), create a web app where you can find Pokemon by type, and create a list of your favourite Pokemon.

Your app will need:

- **A sidebar** with a list of Pokemon types. You should be able to filter the list by typing your query in the search bar. You can get the list of types from the API.
- **Main content section.** When you click a Pokemon type in the sidebar, the app loads all Pokemon of that type from the API and shows them in the main content section. Every Pokemon item should be rendered as a square and include Pokemon name, number (its `id` in the API) and a button for adding it to or removing it from the "My Favourites" section.
- **"My Favourites"** section. When you click the "Star" button on a Pokemon, it should show a modal where you can enter a memo (Mockup #2). After you confirm, the Pokemon appears in the My Favourites section. You can click the "X" button to remove it from favourites. Keep in mind that you can't add a Pokemon to favourites twice.

Notes:

- Every section of the app (sidebar, content and favourites panel) should be scrollable indepentently.

**We evaluate your ability to:**

- Understand task scope, and prioritize to achieve the most functional result within a limited timeframe
- Choose the right tools (packages, libraries, components, etc.)
- Organize the state management and data flow in a complex app
- Organize folder/component structure that is easy to navigate
- Decompose the task into a set of subtasks
- Navigate in API and UI framework with provided documentation
- Write maintainable and understandable code

**We do not evaluate:**

- Pixel perfectness
- Features outside of scope
- Knowledge of quirks, hacks, non-standard behavior (no need for crossbrowser support, etc.)
- Memorized knowledge (feel free to use google, docs, stackoverflow, call a friend...)
- Knowledge of a particular package or library

