import { h } from 'preact';
import { createPortal } from 'preact/compat';
import { useEffect, useRef, useState } from 'preact/hooks';

/**
 * Hooks handles the creation and tear down of the portal's root
 * element.
 *
 * @private
 *
 * @param {string} [id] - The optional id of the target container. If the `id` is not passed,
 * the target container will be a div without an `id`
 *
 * @return {HTMLElement} - The DOM node to use as the Portal target
 */
function usePortal(id: string) {
  const [isAttached, setIsAttached] = useState(false);
  const elRef = useRef(document.getElementById(id) || document.createElement('div'));

  useEffect(() => {
    const el = elRef.current;
    setIsAttached(!!el.parentElement);

    if (!isAttached) {
      el.id = id;
      document.body.appendChild(el);
      setIsAttached(true);
    }

    return () => {
      if (elRef.current.parentElement && isAttached) {
        elRef.current.parentElement.removeChild(el);
      }
    };
  }, [id, elRef, isAttached]);

  return elRef.current;
}

/**
 * Dynamically create's a portal component that appends to the end of the DOM
 *
 * @public
 *
 * @param {object} props - the component's props
 * @param {string} [props.id] - The optional id of the target container. If the `id` is not passed,
 * the target container will be a div without an `id`
 *
 * @param {React.children} props.children - the children element
 *
 * @return {React.ReactPortal}
 */
export const Portal = ({ id, children }: any) => {
  if (typeof document === 'undefined' || !document) {
    return null;
  }

  const root = usePortal(id);

  return createPortal(children, root);
};
