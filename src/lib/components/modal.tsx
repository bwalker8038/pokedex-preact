import { h } from 'preact';
import { useRef } from 'preact/hooks';
import { Portal } from './portal';
import useOutsideClick from '../hooks/use-outside-click';
import useBodyScrollLock from '../hooks/use-body-scroll-lock';
import useTrapFocus from '../hooks/use-trap-focus';
import useHideAriaSiblings from '../hooks/use-hide-aria-siblings';

import './modal.scss';

/**
 * Component renders the internal content of a modal
 *
 * @param {object} props - the component's props
 * @param {React.ReactChildren} props.children - the component's children
 * @param {(val: boolean) => any} props.onDismiss - callback function that onDismisss the
 * show modal state
 *
 * @return {JSX.Element}
 */
const ModalContent = ({ ariaDescribedBy, ariaLabelledBy, children, onDismiss, isOpen }: any) => {
  const modalRef = useRef(null);
  useOutsideClick(modalRef, () => onDismiss(false));
  useTrapFocus(modalRef, isOpen);
  useBodyScrollLock(isOpen);
  useHideAriaSiblings(modalRef, isOpen);

  return (
    <div
      role="dialog"
      aria-modal="true"
      aria-describedby={ariaDescribedBy && ariaDescribedBy}
      aria-labelledby={ariaLabelledBy && ariaLabelledBy}
      class="modal-content"
      ref={modalRef}
    >
      {children}
    </div>
  );
};

/**
 * Component renders a modal and it's wrapper
 *
 * @param {object} props - the components props
 * @param {React.ReactChildren} props.children - the component's children
 * @param {boolean} props.isOpen - flag sets the state of the modal
 * @param {(val: boolean) => void} props.onDismiss - handler that onDismisss the state of the modal
 *
 * @return {JSX.Element}
 */
const Modal = ({ children, onDismiss, ariaLabelledBy, ariaDescribedBy, isOpen = false }: any) => {
  const classes = isOpen ? 'modal-wrapper--active' : 'modal-wrapper';

  /**
   * Handler will set the the modal onDismiss to `false` when the escape key is
   * pressed
   *
   * @private
   *
   * @param {KeyboardEvent} e - the keyboard event
   *
   * @return {void}
   */
  const handleKeyDown = (e: any) => {
    if (['Escape', 'esc'].includes(e.key) || e.keyCode === 27) {
      onDismiss(false);
    }
  };

  return (
    <Portal id="modal-root">
      <div className={classes} onKeyDown={handleKeyDown} aria-hidden={!isOpen} data-testid="base-modal-container">
        {isOpen && (
          <ModalContent
            onDismiss={onDismiss}
            ariaDescribedBy={ariaDescribedBy}
            ariaLabelledBy={ariaLabelledBy}
            isOpen={isOpen}
          >
            {children}
          </ModalContent>
        )}
      </div>
    </Portal>
  );
};

export default Modal;
