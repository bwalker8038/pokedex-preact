/**
 * Method performs a fuzzy search on a set of strings
 *
 * @param needle the string we're trying to match
 * @param haystack - the set of strings we're matching against
 *
 */
export function fuzzysearch(needle: string, haystack: string) {
  const hlen = haystack.length;
  const nlen = needle.length;

  if (nlen > hlen) {
    return false;
  }

  if (nlen === hlen) {
    return needle === haystack;
  }

  outer: for (let i = 0, j = 0; i < nlen; i++) {
    let char = needle.charCodeAt(i);
    while (j < hlen) {
      if (haystack.charCodeAt(j++) === char) {
        continue outer;
      }
    }
    return false;
  }

  return true;
}
