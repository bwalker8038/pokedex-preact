declare module '\*.svg' {
  import React = require('react');
  export const ReactComponent: React.SFC<React.SVGProps<SVGSVGElement>>;
  const src: string;
  export default src;
}

declare type KeyVal<T> = {
  [key: string]: T;
};

declare type PokemonType = {
  id: string;
  name: string;
};

declare type Pokemon = {
  id: string;
  name: string;
  isFavorite: boolean;
  memo: string;
  types: { [typeId: string]: string };
};
