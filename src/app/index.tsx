import { h, Fragment, ComponentChildren } from 'preact';
import { BrowserRouter as Router, Switch, Route, useParams } from 'react-router-dom';
import './app.scss';
import { StoreContext, apiStore } from './pokedex-store';
import CategoryListingView from './views/category-listing-view';
import GridDetailsView from './views/grid-details-view';
import FavoritesView from './views/favorites-view';

const Layout = ({ children }: { children: ComponentChildren }) => (
  <div class="layout">
    <header>
      <h2>Pokedex</h2>
    </header>
    <nav>
      <CategoryListingView />
    </nav>
    <main>{children}</main>
    <aside>
      <FavoritesView />
    </aside>
  </div>
);

/**
 * Component represents the application shell
 *
 */
const App = () => {
  return (
    <StoreContext.Provider value={apiStore}>
      <Router>
        <Layout>
          <Switch>
            <Route path="/" exact render={() => <h2>Select a category!</h2>} />
            <Route path="/types/:id" component={GridDetailsView} />
          </Switch>
        </Layout>
      </Router>
    </StoreContext.Provider>
  );
};

export default App;
