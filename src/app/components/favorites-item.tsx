import { h } from 'preact';
import { useState } from 'preact/hooks';
import close from '../../icons/x.svg';
import './favorites-item.scss';

interface ContextMenuProps {
  isOpen: boolean;
  handleRemoval: () => void;
  handleDimiss: () => void;
}

interface FavoritesItemProps {
  name: string;
  memo: string;
  onUnfavorite: () => void;
}

/**
 * Component render's a context menu when attempting to remove a favorite
 *
 * @param props - the component's props
 * @param props.isOpen - flag represents if the menu is open and visible
 * @param props.handleRemoval - handler fires on removal of the favorite item
 * @param props.handleDismiss - handler fires when the context menu is dismissed
 */
const ContextMenu = ({ isOpen, handleRemoval, handleDimiss }: ContextMenuProps) => {
  const classes = isOpen ? 'context-menu--active' : 'context-menu';

  return (
    <div class={classes}>
      <span>Are you sure you want to delete this item?</span>
      <div class="actions">
        <button onClick={handleRemoval}>Yes</button>
        <button onClick={handleDimiss}>No</button>
      </div>
    </div>
  );
};

/**
 * Component render's a favorited pokemon card
 *
 * @param props - the component's props
 * @param props.name - the name of the pokemon
 * @param props.meno - the memo note on the favorited item
 * @param props.onUnfavorite - callback handles removal of the favorited pokemon
 * from the store
 */
const FavoritesItem = ({ name, memo, onUnfavorite }: FavoritesItemProps) => {
  const [showContextMenu, setShowContextMenu] = useState(false);

  return (
    <article class="favorites-item">
      <header class="favorites-item__header">
        <span>{name}</span>
        <button onClick={() => setShowContextMenu(true)}>
          <span class="a11y-text">Remove pokemon</span>
          <svg>
            <use xlinkHref={close} />
          </svg>
        </button>
      </header>
      {memo && (
        <div class="memo-container">
          <span class="memo-label">Memo: </span>
          <p>{memo}</p>
        </div>
      )}
      <ContextMenu
        isOpen={showContextMenu}
        handleRemoval={() => {
          onUnfavorite();
          setShowContextMenu(false);
        }}
        handleDimiss={() => setShowContextMenu(false)}
      />
    </article>
  );
};

export default FavoritesItem;
