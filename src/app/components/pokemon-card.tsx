import { h } from 'preact';
import star from '../../icons/star.svg';
import './pokemon-card.scss';

interface Props {
  id: string;
  name: string;
  isFavorite: boolean;
  isSelected: boolean;
  handleFavoriteToggle: (id: string) => void;
}
/**
 * Component renders a Pokemon Card
 *
 * @param props - the component's props
 * @param props.id - the id of the pokemon
 * @param props.name - the name of the pokemon
 * @param props.isFavorite - if true, the pokemon was favorited
 * @param props.isSelected - if true, the current pokemon card is focused
 * @param props.handleFavoriteToggle - handler handles favoriting a pokemon
 */
const PokemonCard = ({ id, name, isFavorite, isSelected, handleFavoriteToggle }: Props) => (
  <div class={`pokemon-card ${isSelected ? 'pokemon-card--selected' : ''}`}>
    <button class={`${isFavorite ? 'favorited' : ''}`} onClick={() => handleFavoriteToggle(id)}>
      <span class="a11y-text">Add to favorites</span>
      <svg>
        <use xlinkHref={star} />
      </svg>
    </button>
    <div class="info-container">
      <h3>{name}</h3>
      <span class="id-tag">{`#${id}`}</span>
    </div>
  </div>
);

export default PokemonCard;
