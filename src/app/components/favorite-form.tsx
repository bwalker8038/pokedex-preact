import { h, Fragment } from 'preact';
import { useState } from 'preact/hooks';
import './favorite-form.scss';

interface Props {
  favoriteHandler: (memo: string) => void;
  closeModalHandler: () => void;
}

const FavoriteForm = ({ favoriteHandler, closeModalHandler }: Props) => {
  const [memo, setMemo] = useState(null);

  return (
    <div class="favorite-form">
      <h3>Add to Favorites</h3>
      <form
        onSubmit={(e) => {
          e.preventDefault();
          favoriteHandler(memo);
          closeModalHandler();

          return false;
        }}
      >
        <label for="memo-input">
          <span>Add a memo if you wish:</span>
          <input
            id="memo-input"
            type="text"
            onChange={(e) => setMemo((e.target as HTMLInputElement).value)}
            placeholder="This is my favorite..."
          />
        </label>
        <div class="actions">
          <button class="cancel" onClick={closeModalHandler}>
            Cancel
          </button>
          <button class="submit" type="submit">
            Add
          </button>
        </div>
      </form>
    </div>
  );
};

export default FavoriteForm;
