import { h } from 'preact';
import { useRef } from 'preact/hooks';
import close from '../../icons/x.svg';
import './search-input.scss';

interface Props {
  onChangeHandler: (value: string) => void;
}
/**
 * Component renders a search input
 *
 * @param props - The component's prop object
 * @param props.onChangeHandler - function handles an input change
 *
 */
const SearchInput = ({ onChangeHandler }: Props) => {
  const inputRef = useRef(null);

  return (
    <div class="search-input">
      <input
        type="search"
        placeholder="Enter a type..."
        onChange={(e) => onChangeHandler((e.target as HTMLInputElement).value)}
        ref={inputRef}
      />
      <button
        onClick={(e) => {
          onChangeHandler('');
          (inputRef.current as HTMLInputElement).value = '';
        }}
      >
        <span class="a11y-text">Clear</span>
        <svg>
          <use xlinkHref={close} />
        </svg>
      </button>
    </div>
  );
};

export default SearchInput;
