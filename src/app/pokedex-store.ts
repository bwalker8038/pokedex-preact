import { createContext } from 'preact';
import { Observable } from '../lib/observable';
import axios from 'axios';

const BASE_API_URL = 'https://pokeapi.co/api/v2';

/**
 * Helper function extracts an id from a url;
 * @param url - the url that contains the id
 */
function getIdFromUrl(url: string) {
  const regexp = /(\/)(\d+)(\/)/g;
  const [raw] = url.match(regexp);
  const id = raw.replace(/\//g, '');

  return id ? id : undefined;
}

/**
 * Helper function capitalizes the first letter of a string
 *
 * @param str - the string to be capitalized
 */
function capitalize(str: string) {
  return `${str.charAt(0).toUpperCase()}${str.slice(1)}`;
}

// Cache objects
let pokemonCache = new Observable<{ [id: string]: Pokemon }>({});
let categoryCache = new Observable<{ [id: string]: PokemonType }>({});
let categoryIndex = {} as any;

/**
 * Service fetches, stores and updates both pokemon types and the pokemon themselves
 */
class APIStore {
  public pokemonCache = pokemonCache;
  public typeCache = categoryIndex;
  public categoryCache = categoryCache;

  /**
   * Method fetches pokemon types from the API, stores the results in the
   * category cache observable
   */
  async fetchPokemonTypes() {
    try {
      const endpoint = `${BASE_API_URL}/type/`;
      const { data } = await axios(endpoint);

      for (let { name, url } of data.results) {
        const id = getIdFromUrl(url);
        categoryCache.set({ ...categoryCache.get(), [id]: { name: capitalize(name), id } });
      }
    } catch (e) {
      console.log(e);
    }
  }

  /**
   * Method fetches pokemon for a specified type, store the pokemone cache observable
   *
   * @param typeId - the pokemon type id
   */
  async fetchPokemonByTypeId(typeId: string) {
    // Don't attempt to do a network fetch if we've already seen the
    // category
    if (categoryIndex[typeId]) return;

    try {
      const endpoint = `${BASE_API_URL}/type/${typeId}`;
      const { data } = await axios(endpoint);

      for (const { pokemon } of data.pokemon) {
        const { name, url } = pokemon;
        const id = getIdFromUrl(url);

        let payload;

        if (pokemonCache.get()[id]) {
          payload = pokemonCache.get()[id];
        } else {
          payload = { id, name: capitalize(name), memo: '', isFavorite: false, types: { [typeId]: typeId } };
          pokemonCache.set({ ...pokemonCache.get(), [id]: payload });
          pokemonCache.get()[id] = payload;

          // index of seen pokemon types and pokemon ids
          if (!categoryIndex[typeId]) categoryIndex[typeId] = [];
          categoryIndex[typeId].push(id);
        }
      }
    } catch (e) {
      console.log(e);
    }
  }

  /**
   * Method updates a specified pokemon record
   *
   * @param type - the action type to be performed
   * @param payload - the payload of the action
   * @param payload.id - the pokemon's id
   * @param payload.memo- the pokemon's memo
   */
  updatePokemon(type: string, { id, memo }: any) {
    const copy = pokemonCache.get()[id];
    switch (type) {
      case 'TOGGLE_FAVORITE':
        copy.isFavorite = !copy.isFavorite;
        break;

      case 'UPDATE_MEMO':
        copy.memo = memo;
        break;

      case 'UPDATE_MEMO_AND_TOGGLE_FAVORITE':
        copy.memo = memo;
        copy.isFavorite = !copy.isFavorite;
        break;

      default:
        console.log('invalid type!');
    }

    pokemonCache.set({ ...pokemonCache.get(), [id]: copy });
    return copy;
  }
}

// Expose the store as a singleton service
export const apiStore = new APIStore();

// Expose the service
export const StoreContext = createContext(apiStore);
