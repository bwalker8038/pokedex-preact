import { h, Fragment } from 'preact';
import { NavLink } from 'react-router-dom';
import { useState, useEffect, useContext } from 'preact/hooks';
import { fuzzysearch } from '../../lib/fuzzy';
import SearchInput from '../components/search-input';
import { StoreContext } from '../pokedex-store';
import { useObservable } from '../../lib/observable';
import './category-listing-view.scss';

/**
 * Component renders a category list
 */
const CategoryList = ({ types }: { types: PokemonType[] }) => {
  const items = types.map(({ name, id }) => (
    <li key={`category-${id}`} class="category-list__item">
      <NavLink to={`/types/${id}`} activeClassName="active">
        {name}
      </NavLink>
    </li>
  ));

  return <ul class="category-list">{items}</ul>;
};

/**
 * Component renders the category listing view
 */
const CategoryListingView = () => {
  const [searchQuery, setSearchQuery] = useState('');
  const { fetchPokemonTypes, categoryCache } = useContext(StoreContext);
  const typeData = useObservable(categoryCache);
  const types = Object.keys(typeData).map((id) => typeData[id]);

  useEffect(() => {
    const fetchPayload = async () => {
      await fetchPokemonTypes();
    };

    fetchPayload();
  }, []);

  const filteredTypes = types.filter(({ name }: Pokemon) => fuzzysearch(searchQuery.toLowerCase(), name.toLowerCase()));
  return (
    <Fragment>
      <header class="nav-header">
        <SearchInput onChangeHandler={setSearchQuery} />
      </header>
      <CategoryList types={filteredTypes} />
    </Fragment>
  );
};

export default CategoryListingView;
