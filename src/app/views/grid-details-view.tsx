import { h, Fragment } from 'preact';
import { useEffect, useContext, useState } from 'preact/hooks';
import { StoreContext } from '../pokedex-store';
import { useParams } from 'react-router-dom';
import PokemonCard from '../components/pokemon-card';
import { useObservable } from '../../lib/observable';
import Modal from '../../lib/components/modal';

import './grid-details-view.scss';
import FavoriteForm from '../components/favorite-form';

/**
 * Function filters the pokemon cache by typeId
 *
 * @param pokemon - the pokemon cache to be filtered
 * @param typeId - the id of the pokemon type
 */
function filter(pokemon: { [id: string]: Pokemon }, typeId: string) {
  const ids = Object.keys(pokemon);

  return ids.map((id) => pokemon[id]).filter(({ types }: any) => types[typeId]);
}

const GridDetailsView = () => {
  const { id } = useParams();
  const { fetchPokemonByTypeId, updatePokemon, pokemonCache, categoryCache } = useContext(StoreContext);

  const typeData = useObservable(categoryCache);
  const pokemon = useObservable(pokemonCache);
  const currentPokemon = filter(pokemon, id);

  const [isModalOpen, setIsModalOpen] = useState(false);
  const [currentId, setCurrentId] = useState(null);

  // side effect will do a network fetch to seed the cache
  useEffect(() => {
    const fetchPayload = async () => {
      if (id) await fetchPokemonByTypeId(id);
    };

    fetchPayload();
  }, [id]);

  // Function handles favoriting a pokemon
  function favoriteHandler(id: string, memo: string) {
    updatePokemon('UPDATE_MEMO_AND_TOGGLE_FAVORITE', { id, memo });
    setCurrentId(null);
  }

  // Function handles toggling the modal
  function handleFavoriteToggle(id: string, isFavorite: boolean) {
    // If the pokemon was already favorited, unfavorite it
    if (isFavorite) return updatePokemon('TOGGLE_FAVORITE', { id });

    setIsModalOpen(true);
    setCurrentId(id);
  }

  // Function handles dismissing the modal
  function handleDismissModal() {
    setIsModalOpen(false);
    setCurrentId(null);
  }

  const cards =
    currentPokemon &&
    currentPokemon.map(({ name, id, isFavorite }: Pokemon) => {
      return (
        <PokemonCard
          key={`pokemon-${id}`}
          name={name}
          id={id}
          isSelected={id === currentId}
          isFavorite={isFavorite}
          handleFavoriteToggle={() => handleFavoriteToggle(id, isFavorite)}
        />
      );
    });

  return (
    <Fragment>
      <h1 class="grid-view__header">{typeData[id]?.name}</h1>
      <div class="grid-view">{cards}</div>
      <Modal isOpen={isModalOpen} onDismiss={handleDismissModal}>
        <FavoriteForm
          favoriteHandler={(memo: string) => favoriteHandler(currentId, memo)}
          closeModalHandler={handleDismissModal}
        />
      </Modal>
    </Fragment>
  );
};

export default GridDetailsView;
