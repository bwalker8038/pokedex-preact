import { h } from 'preact';
import { useObservable } from '../../lib/observable';
import { useContext } from 'preact/hooks';
import { StoreContext } from '../pokedex-store';
import FavoritesItem from '../components/favorites-item';
import './favorites-view.scss';

/**
 * Function filters a object of pokemon by `isFavorite`
 *
 * @param pokemon - the pokemon cache to be filtered
 */
function filter(pokemon: { [id: string]: Pokemon }) {
  const ids = Object.keys(pokemon);

  return ids.map((id) => pokemon[id]).filter(({ isFavorite }: Pokemon) => isFavorite === true);
}

/**
 * Component renders the Favorites View
 */
const FavoritesView = () => {
  const { updatePokemon, pokemonCache } = useContext(StoreContext);

  const pokemon = useObservable(pokemonCache);
  const favorites = filter(pokemon);

  // Function handles unfavoriting a pokemon
  function handleUnfavorite(id: string) {
    updatePokemon('UPDATE_MEMO_AND_TOGGLE_FAVORITE', { id, memo: null });
  }

  const cards = favorites.map(({ name, id, memo }: Pokemon) => (
    <FavoritesItem key={`favorite-${id}`} name={name} memo={memo} onUnfavorite={() => handleUnfavorite(id)} />
  ));

  const hasCards = cards.length > 0;
  const listClass = hasCards ? 'cards-list' : 'cards-list--empty';

  return (
    <div class="favorites-view">
      <h3>My Favorites</h3>
      <div class={listClass}>{hasCards ? cards : <span>No items here</span>}</div>
    </div>
  );
};

export default FavoritesView;
