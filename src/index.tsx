import { h, render } from 'preact';
import App from './app/index';

render(<App />, document.getElementById('root'));
